import 'package:flutter/material.dart';
import './textcontrol.dart';

class TextOutput extends StatefulWidget{
  @override
  _TextOutputState createState() => _TextOutputState();
}

class _TextOutputState extends State<TextOutput>{
  String msg = 'Blouse Panjang';

  void _changeText() {
    setState(() {
      if (msg.startsWith('K')) {
        msg = 'Blouse Panjang';
      }else{
        msg = 'Kemeja Pria';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Warna Warni Shop ', style: new TextStyle(fontSize:30.0),),
          TextControl(msg),
          RaisedButton(child: Text("Pilih Barang",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}